package thm.softwarearchitecture;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DbServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
      // code to call DatabaseAgent
      // Dieses Servlet nimmt die übermittelten Daten und übergibt sie an das DAO.
      String customerName = request.getParameter("customerName");
      String cityName = request.getParameter("contactCity");

      // do more stuff

      PrintWriter out = response.getWriter();
      out.println(customerName + ", located in "+ cityName + " was added. to Database");
      out.flush();
      out.close();
    }
    
}
